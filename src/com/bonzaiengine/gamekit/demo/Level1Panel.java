/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo;

import com.bonzaiengine.gamekit.demo.script.GameManager;
import com.bonzaiengine.io.event.ActionEvent;
import com.bonzaiengine.io.event.ActionListener;
import com.bonzaiengine.ui.widget.Label;
import com.bonzaiengine.ui.widget.Panel;
import com.bonzaiengine.ui.widget.Widget;

/**
 * assets/level1.ui.xml
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
public class Level1Panel extends Panel {
	private GameManager gameManager;
	
	private Label starCount;
	private Widget life1, life2, life3;
	private Panel pausePanel;
	private Label pauseResume, pauseRetry, pauseExit;
	private Panel diePanel;
	private Label dieRetry, dieExit;
	private Panel winPanel;
	private Label winExit;
	
	public Level1Panel() {
		super();
	}
	
	// Called during ui file loading due to: <field name="forceInit" />
	public final void setForceInit() {
		starCount = find("starCount");
		life1 = find("life1");
		life2 = find("life2");
		life3 = find("life3");
		pausePanel = find("pausePanel");
		pauseResume = find("pauseResume");
		pauseRetry = find("pauseRetry");
		pauseExit = find("pauseExit");
		diePanel = find("diePanel");
		dieRetry = find("dieRetry");
		dieExit = find("dieExit");
		winPanel = find("winPanel");
		winExit = find("winExit");
		
		// Connect listener to 
		final ActionListener resumeLevel = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gameManager.setPaused(false);
				hidePanels();
			}
		};
		final ActionListener retryLevel = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gameManager.retryLevel();
				hidePanels();
			}
		};
		final ActionListener exitLevel = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gameManager.exit();
				hidePanels();
			}
		};
		
		pauseResume.addActionListener(resumeLevel);
		pauseRetry.addActionListener(retryLevel);
		pauseExit.addActionListener(exitLevel);
		dieRetry.addActionListener(retryLevel);
		dieExit.addActionListener(exitLevel);
		winExit.addActionListener(exitLevel);
	}

	protected void hidePanels() {
		pausePanel.setVisible(false);
		diePanel.setVisible(false);
	}
	
	/* */
	
	public void setGameManager(GameManager gameManager) {
		this.gameManager = gameManager;
	}

	public void setNumLives(int numLives) {
		life1.setVisible(numLives >= 1);
		life2.setVisible(numLives >= 2);
		life3.setVisible(numLives >= 3);
	}

	public void setNumBonus(int numBonus) {
		starCount.setText(Integer.toString(numBonus));
	}
	
	public void setPaused(boolean pause) {
		pausePanel.setVisible(pause);
	}
	public void death() {
		diePanel.setVisible(true);
	}
	public void win() {
		winPanel.setVisible(true);
	}
}
