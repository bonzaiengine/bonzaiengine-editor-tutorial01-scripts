/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import com.bonzaiengine.gamekit.GameObject;
import com.bonzaiengine.gamekit.Message;
import com.bonzaiengine.gamekit.component.ISensorListener;
import com.bonzaiengine.gamekit.component.ScriptComponent;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
public class CheckPoint extends ScriptComponent implements ISensorListener {
	public Message enterMessage;
	
	@Override
	public void onEnter(GameObject entered) {
		Health health = entered.getComponent(Health.class);
		if(health != null) {
			health.setCheckpoint(getGameObject());
			
			Message message = enterMessage;
			if(message != null) {
				message.send();
			}
		}
	}
	@Override
	public void onExit(GameObject exited) {}
}
