/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import static com.bonzaiengine.math.MathFunc.clamp;
import static com.bonzaiengine.math.MathFunc.interpolate;

import com.bonzaiengine.gamekit.annotation.RequireComponent;
import com.bonzaiengine.gamekit.annotation.SerializableField;
import com.bonzaiengine.gamekit.component.CharacterControllerComponent;
import com.bonzaiengine.gamekit.component.ScriptComponent;
import com.bonzaiengine.io.event.KeyEvent;
import com.bonzaiengine.io.input.IInput;
import com.bonzaiengine.math.Vec3;
import com.bonzaiengine.physics.CollisionObject;
import com.bonzaiengine.physics.RigidBody;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
@RequireComponent(CharacterControllerComponent.class)
public class CharacterMover extends ScriptComponent {
	private static final float SPEEDFACTOR_START = 0.2f;
	private static final float SPEEDFACTOR_TIMERAMP = 0.4f;

	@SerializableField(value="Speed", desc="")
	public float speed = 3.0f;
	@SerializableField(value="Jump impulse", desc="")
	public float jumpImpulse = 6.5f;
	
	private float speedFactor, speedTime;
	private int speedDir;
	
	private boolean left, right, jump;
	private CharacterControllerComponent controller;
	
	@Override
	public void init() {
		controller = getGameObject().getComponent(CharacterControllerComponent.class);
		controller.getCharacterController().setJumpImpulse(jumpImpulse);

		for(CollisionObject collisionObject : controller.getCharacterController().getCollisionObjects()) {
			if(collisionObject.getClass() == RigidBody.class) {
				RigidBody rigidBody = (RigidBody)collisionObject;
				rigidBody.setLinearFactor(new Vec3(1, 1, 0));
				rigidBody.setAngularFactor(new Vec3(0, 0, 1));
			}
		}
	}
	
	@Override
	public void updateStep(long timeStepMs) {
	}
	
	@Override
	public void processInputs(IInput input) {
		left = (input.isKeyPressed(KeyEvent.VK_LEFT) > 0);
		right = (input.isKeyPressed(KeyEvent.VK_RIGHT) > 0);
		jump = (input.isKeyPressed(KeyEvent.VK_UP) > 0);
		if(left && right) {
			left = false;
			right = false;
		}
		
		int lastDir = speedDir;
		speedDir = (left ? -1 : (right ? 1 : 0));
		if(lastDir != speedDir) {
			speedTime = 0;
		}
	}
	
	@Override
	public void update(long timePassedMs) {
		speedTime += timePassedMs / 1000f;
		speedFactor = interpolate(SPEEDFACTOR_START, 1, clamp(speedTime / SPEEDFACTOR_TIMERAMP, 0, 1));
		
		float speed = this.speed * speedFactor;
		speed = (right ? speed : (left ? -speed : 0));
		controller.getCharacterController().move(Vec3.axisX, speed);
		
		if(jump) {
			controller.getCharacterController().jump();
		}
	}
}
