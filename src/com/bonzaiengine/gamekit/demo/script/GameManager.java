/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import com.bonzaiengine.gamekit.GameObject;
import com.bonzaiengine.gamekit.GameScreen;
import com.bonzaiengine.gamekit.annotation.RequireComponent;
import com.bonzaiengine.gamekit.component.CollisionShapeComponent;
import com.bonzaiengine.gamekit.component.ModelComponent;
import com.bonzaiengine.gamekit.component.RigidBodyComponent;
import com.bonzaiengine.gamekit.component.ScriptComponent;
import com.bonzaiengine.gamekit.component.UiComponent;
import com.bonzaiengine.gamekit.demo.Level1Panel;
import com.bonzaiengine.io.event.KeyEvent;
import com.bonzaiengine.io.input.IInput;
import com.bonzaiengine.math.Transform;
import com.bonzaiengine.math.Vec3;
import com.bonzaiengine.model.ModelInstance;
import com.bonzaiengine.model.asset.AssetManager;
import com.bonzaiengine.model.asset.AsyncModel;
import com.bonzaiengine.physics.PhysicWorld;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
@RequireComponent(UiComponent.class)
public class GameManager extends ScriptComponent {
	public static final boolean DEBUG = true;

	private GameObject player;
	private GameObject physic;
	private GameObject cameraFollower;
	private GameObject cameraFree;
	private UiComponent ui;
	
	private boolean pause = false;
	
	@Override
	public void init() {
		GameObject gameObject = getGameObject();
		GameScreen level = gameObject.getGameScreen();
		
		ui = gameObject.getComponent(UiComponent.class);
		player = level.find("player");
		physic = getGameObject().getGameScreen().find("physic");
		cameraFollower = level.find("cameraFollower");
		cameraFree = level.find("cameraFree");
	}
	@Override
	public void start() {
		Level1Panel uiPanel = getUiPanel();
		uiPanel.setGameManager(this);
		
		Health health = player.getComponent(Health.class);
		health.setNumLives(3);
		
		GameObject checkPoint = getGameObject().getGameScreen().find("checkpoint0");
		health.setCheckpoint(checkPoint);
		health.spawnAtCheckpoint();
	}
	
	@Override
	public void processInputs(IInput input) {
		if(input.isKeyPressed(KeyEvent.VK_ESCAPE) > 0) {
			setPaused(true);
		}
		if(DEBUG) {
			if(input.isKeyClicked(KeyEvent.VK_C) > 0) {
				if(cameraFree != null) {
					if(cameraFollower.isNodeActive()) {
						cameraFollower.setNodeActive(false);
						cameraFree.setNodeActive(true);
						
						Transform t = new Transform();
						cameraFollower.getWorldTransform(t);
						cameraFree.setWorldTransform(t);
					}
					else {
						cameraFollower.setNodeActive(true);
						cameraFree.setNodeActive(false);
					}
					
					syncActiveStates();
				}
			}
			if(input.isKeyClicked(KeyEvent.VK_P) > 0) {
				GameScreen gameScreen = getGameObject().getGameScreen();
				gameScreen.setDebugRenderPhysic(!gameScreen.isDebugRenderPhysic());
			}
			if(input.isKeyClicked(KeyEvent.VK_S) > 0) {
				if((cameraFree != null) && cameraFree.isNodeActive()) {
					Transform t = new Transform();
					cameraFree.getWorldTransform(t);
					
					Vec3 pos = new Vec3();
					t.getPosition(pos);
					
					player.getWorldTransform(t);
					
					Vec3 pos2 = new Vec3();
					t.getPosition(pos2);
					pos2.set(pos.x, pos.y + 3, pos2.z);
					t.setPosition(pos2);
					
					player.getComponent(Health.class).spawnAt(t);
				}
			}
			if(input.isKeyClicked(KeyEvent.VK_B) > 0) {
				final PhysicWorld physicWorld = getGameObject().getGameScreen().getPhysic();
				
				Transform t = new Transform(); // TODO
				Vec3 position = new Vec3();
				t.getPosition(position);
				position.y += 3f;
				
				player.getWorldTransform(t);
				t.setPosition(position);
				
				AssetManager assets = getGameObject().getGameScreen().getAssets();
				AsyncModel cube = assets.models.get("cube");
				
				float w = 1, h = 1;
				GameObject go = new GameObject();
				go.setName("fallingBox");
				ModelInstance instance = new ModelInstance(go.getName());
				instance.instanciate(cube);
				instance.transform(new Transform().setScale(new Vec3(w, h, 1)));
				ModelComponent model = new ModelComponent();
				model.modelInstance = instance;
				CollisionShapeComponent collision = new CollisionShapeComponent();
				collision.shape = physicWorld.createBoxCollisionShape(new Vec3(w, h, 1));
				RigidBodyComponent rigidBody = new RigidBodyComponent();
				rigidBody.mass = 10;
				set2dMask(rigidBody);
				go.setTransform(t);
				go.renderable = model;
				go.collision = collision;
				go.rigidBody = rigidBody;
				
				physic.addChild(go);
			}
		}
	}
	
	private void syncActiveStates() {
		boolean gamePaused = pause || !cameraFollower.isNodeActive();
		player.setNodeActive(!gamePaused);
		physic.setNodeActive(!gamePaused);
	}

	@Override
	public void updateEnd(long timePassedMs) {
		Health health = player.getComponent(Health.class);
		BonusCollector collector = player.getComponent(BonusCollector.class);
		
		Level1Panel uiPanel = getUiPanel();
		uiPanel.setNumLives(health.getNumLives());
		uiPanel.setNumBonus(collector.getNumBonus());
	}
	
	private final Level1Panel getUiPanel() {
		Level1Panel uiPanel = (Level1Panel)ui.getWidget();
		return uiPanel;
	}
	
	public final void setPaused(boolean paused) {
		if(this.pause != paused) {
			this.pause = paused;
			
			Level1Panel uiPanel = getUiPanel();
			uiPanel.setPaused(this.pause);
			
			syncActiveStates();
		}
	}
	
	public final void onDeath() {
		Level1Panel uiPanel = getUiPanel();
		uiPanel.death();
	}
	public final void onWin() {
		Level1Panel uiPanel = getUiPanel();
		uiPanel.win();
	}
	
	public void retryLevel() {
		getGameObject().getGameScreen().reload();
	}
	
	public void exit() {
		getGameObject().getGameScreen().getGame().requestExit();
	}
	
	public static final void set2dMask(RigidBodyComponent rigidBody) {
		rigidBody.translationMask[2] = false;
		rigidBody.rotationMask[0] = false;
		rigidBody.rotationMask[1] = false;
	}
}
