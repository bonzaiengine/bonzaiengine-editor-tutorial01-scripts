/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import com.bonzaiengine.gamekit.component.ScriptComponent;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
public class BonusCollector extends ScriptComponent {
	private int numBonus;
	
	public int getNumBonus() {
		return numBonus;
	}
	public void setNumBonus(int numBonus) {
		this.numBonus = numBonus;
	}
	
	public void collect(int numBonus) {
		this.numBonus += numBonus;
	}
}
