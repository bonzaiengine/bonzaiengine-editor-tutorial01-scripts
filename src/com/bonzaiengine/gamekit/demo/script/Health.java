/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import com.bonzaiengine.gamekit.GameObject;
import com.bonzaiengine.gamekit.Message;
import com.bonzaiengine.gamekit.annotation.SerializableField;
import com.bonzaiengine.gamekit.component.ScriptComponent;
import com.bonzaiengine.math.Transform;
import com.bonzaiengine.math.Vec3;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
public class Health extends ScriptComponent {
	private int numLives;
	private GameObject checkpoint;
	@SerializableField(value="Death message", desc="")
	public Message deathMessage;
	private boolean deathSend;
	
	private static final Transform transform = new Transform();
	private static final Vec3 vec3 = new Vec3();
	
	public Health() {
		this.numLives = 1;
	}
	
	@Override
	public void init() {
	}
	
	@Override
	public void updateEnd(long timeStepMs) {
		getGameObject().getWorldTransform(transform);
		float y = transform.getPosition(vec3).y;
		if(y < -1f) {
			onDeath();
		}
	}
	
	// TODO [gameKit/demo] detect death due to kinematic platform
	private final void onDeath() {
		if(numLives > 1) {
			numLives--;
			spawnAtCheckpoint();
		}
		else {
			numLives = 0;
			if(!deathSend && (deathMessage != null)) {
				deathSend = true;
				deathMessage.send();
			}
		}
	}

	public void setNumLives(int numLives) {
		this.numLives = numLives;
	}
	public void setCheckpoint(GameObject gameObject) {
		checkpoint = gameObject;
	}
	public void spawnAtCheckpoint() {
		Transform at = new Transform();
		checkpoint.getWorldTransform(at);
		spawnAt(at);
	}
	public void spawnAt(Transform t) {
		getGameObject().setWorldTransform(t);
	}
	
	public int getNumLives() {
		return numLives;
	}
}
