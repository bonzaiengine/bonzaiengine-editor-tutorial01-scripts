/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import com.bonzaiengine.gamekit.GameObject;
import com.bonzaiengine.gamekit.component.ISensorListener;
import com.bonzaiengine.gamekit.component.ScriptComponent;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
public class Bonus extends ScriptComponent implements ISensorListener {
	private int numBonus = 1;
	
	@Override
	public void onEnter(GameObject entered) {
		BonusCollector collector = entered.getComponent(BonusCollector.class);
		if(collector != null) {
			getGameObject().destroy(); // Collect once
			
			collector.collect(numBonus);
		}
	}
	@Override
	public void onExit(GameObject exited) {}
}
