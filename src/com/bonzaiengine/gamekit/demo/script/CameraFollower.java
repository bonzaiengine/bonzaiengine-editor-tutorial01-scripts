/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import static com.bonzaiengine.math.MathFunc.abs;
import static com.bonzaiengine.math.MathFunc.interpolate;
import static com.bonzaiengine.math.MathFunc.min;
import static com.bonzaiengine.math.MathFunc.sign;

import com.bonzaiengine.gamekit.GameObject;
import com.bonzaiengine.gamekit.annotation.SerializableField;
import com.bonzaiengine.gamekit.component.ScriptComponent;
import com.bonzaiengine.math.Transform;
import com.bonzaiengine.math.Vec2;
import com.bonzaiengine.math.Vec3;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
public class CameraFollower extends ScriptComponent {
	private static final Vec2 MOVEX_WINDOW = new Vec2(-0.5f, 0.5f);
	private static final Vec2 MOVEY_WINDOW = new Vec2(0.f, 1.0f);
	private static final float SPEED_THRESHOLD = 0.15f;

	@SerializableField(value="Follows", desc="")
	public GameObject follows;
	@SerializableField(value="Speed", desc="")
	public float speed = 15f;

	private final Vec3 offset = new Vec3();
	private final Vec3 curSpeed = new Vec3();
	
	private static final Transform t = new Transform();
	private static final Vec3 source = new Vec3();
	private static final Vec3 target = new Vec3();
	
	@Override
	public void init() {
		final GameObject gameObject = getGameObject();
		gameObject.getWorldTransform(t);
		t.getPosition(offset);
	}
	
	@Override
	public void updateEnd(long timePassedMs) {
		GameObject follows = this.follows;
		if(follows == null) {
			return;
		}
		
		GameObject gameObject = getGameObject();
		float timePassedS = timePassedMs / 1000f;
		
		// Followed game object
		follows.getWorldTransform(t);
		t.getPosition(target);
		target.add(offset);
		// Update x pos
		gameObject.getWorldTransform(t);
		t.getPosition(source);
		smooth(source, target, curSpeed, timePassedS);
		t.setPosition(source);
		gameObject.setWorldTransform(t);
	}
	
	private final void smooth(Vec3 source, Vec3 target, Vec3 curSpeed, float dtS) {
		float invDtS = 1f / dtS;
		
		float srcX = source.x;
		float tgtX = target.x;
		float newX = smooth(srcX, tgtX, curSpeed.x, dtS, MOVEX_WINDOW);
		curSpeed.x = abs(newX - srcX) * invDtS;
		source.x = newX;
		
		float srcY = source.y;
		float tgtY = target.y;
		float newY = smooth(srcY, tgtY, curSpeed.y, dtS, MOVEY_WINDOW);
		curSpeed.y = abs(newY - srcY) * invDtS;
		source.y = newY;
	}
	private final float smooth(float source, float target, float curSpeed, float dtS, Vec2 distWindow) {
		float delta = (target - source);
		float sign = sign(delta);
		target -= (delta > 0) ? distWindow.y : distWindow.x;
		delta = sign * (target - source);
		
		boolean move = (curSpeed != 0);
		if(delta > 0) {
			move = true;
		}
		else if(curSpeed <= SPEED_THRESHOLD) {
			move = false;
		}
		float percent = move ? min(speed * dtS, 1) : 0;
		float newSource = interpolate(source, target, percent);
		newSource = interpolate(source, newSource, percent);
		return newSource;
	}
}
