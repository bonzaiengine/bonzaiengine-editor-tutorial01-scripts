/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import com.bonzaiengine.gamekit.annotation.SerializableField;
import com.bonzaiengine.gamekit.component.ScriptComponent;
import com.bonzaiengine.math.Transform;
import com.bonzaiengine.math.Vec3;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
public class LinearMover extends ScriptComponent {
	@SerializableField(value="Start", desc="")
	public final Vec3 start = new Vec3();
	@SerializableField(value="End", desc="")
	public final Vec3 end = new Vec3();
	@SerializableField(value="Speed", desc="")
	public float speed = 1f;
	
	private final Transform transform = new Transform();
	private final Vec3 origin = new Vec3();
	private final Vec3 pos = new Vec3();
	private float t;
	private int dir;
	
	@Override
	public void init() {
		t = 0;
		dir = 1;
	}
	
	@Override
	public void onActivate() {
		getGameObject().getTransform(transform);
		transform.getPosition(origin);
	}
	
	@Override
	public void updateStep(long timeStepMs) {
		t += speed * (timeStepMs / 1000f) * dir;
		if(t < 0) {
			t = -t;
			dir = 1;
		}
		else if(t > 1) {
			t = 1 - (t - 1);
			dir = -1;
		}
		
		pos.interpolate(start, end, t);
		pos.add(origin);
		transform.setPosition(pos);
		getGameObject().setTransform(transform);
	}
}
