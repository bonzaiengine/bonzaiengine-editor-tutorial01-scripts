/* $LICENSE$ */
package com.bonzaiengine.gamekit.demo.script;

import com.bonzaiengine.gamekit.GameObject;
import com.bonzaiengine.gamekit.component.ScriptComponent;
import com.bonzaiengine.gamekit.util.FreeCamera;
import com.bonzaiengine.io.input.IInput;
import com.bonzaiengine.math.ITransformable;
import com.bonzaiengine.math.Transform;
import com.bonzaiengine.memory.MemoryAllocator;
import com.bonzaiengine.scene.SceneView;

/**
 * @copyright_notice Bonzai Engine, bonzaiengine.com, Jérôme JOUVIE
 */
public class CameraMover extends ScriptComponent {
	private FreeCamera freeCamera;
	
	@Override
	public void init() {
		freeCamera = new FreeCamera(new ITransformable() {
			final GameObject gameObject = getGameObject();
			
			/* ITransformable */
			@Override
			public final Transform getTransform(Transform dst) {
				return gameObject.getWorldTransform(dst);
			}
			@Override
			public final void setTransform(Transform src) {
				gameObject.setWorldTransform(src);
			}
			@Override
			public final void transform(Transform src) {
				Transform t = MemoryAllocator.allocTransform();
				gameObject.getWorldTransform(t);
				t.transform(src);
				gameObject.setWorldTransform(t);
				MemoryAllocator.free(t);
			}
		});
		freeCamera.setUsePointerControl(false);
		freeCamera.setTranslationMask(0, true);
		freeCamera.setTranslationMask(1, true);
		freeCamera.setTranslationMask(2, false);
		freeCamera.setRotationMask(0, false);
		freeCamera.setRotationMask(1, false);
		freeCamera.setRotationMask(2, false);
	}
	
	@Override
	public void processInputs(IInput input) {
		final SceneView sceneView = getGameObject().getGameScreen().getGame().getSceneView();
		freeCamera.processInput(sceneView, input);
	}
	
	@Override
	public void update(long timePassedMs) {
		freeCamera.update(timePassedMs);
	}
}
