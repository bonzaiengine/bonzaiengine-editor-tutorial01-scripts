Bonzai Engine Editor - Tutorial 01 (scripts)

First tutorial to learn the basis of the Bonzai Engine Editor.
Scripts for the project available on the repository 'bonzaiengine-editor-tutorial01'.
Based on Bonzai Engine 1.3.2 and above.

http://bonzaiengine.com